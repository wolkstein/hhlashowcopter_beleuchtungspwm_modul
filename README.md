# README #
Ein einfaches Modul auf Basis eines Arduino Nano um ein festeingestelltes PWM(Servo) Signal auszugeben
![picture](Bilder/draufsicht.jpg)

## Hardware ##
* Arduino Nano V3 (Az-Delivery)
* D3 Festeingestelltes PWM bei 1600us
# Anschlüsse #
## Servostecker ##
* 1x Schwarz GND
* 1x Rot +5V Parallel zu Stromversorgung
* 1x Weiß Signal bei 5V TTL Level
## Stecker Stromversorgung ##
### Ausführung Jst 2Polig ###
* 1x Schwarz GND
* 1x Rot 5v Nennspannung, Max 5,2V

Info PWM: Das festeingestellte PWM muß in der Firmware geändert werden